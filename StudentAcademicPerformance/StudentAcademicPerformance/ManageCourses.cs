﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace StudentAcademicPerformance
{
    public partial class ManageCourses : Form
    {
        private const string ConnectionString = @"Data Source=(local);Initial Catalog=StudentAcademicPerformance;Integrated Security=True";

        public ManageCourses()
        {
            InitializeComponent();
            InitializePictureBoxes();
            LoadDataIntoDataGridView();
        }
        private void InitializePictureBoxes()
        {
            pictureBox1.Image = Image.FromFile("clerk-min.png");
            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBox1.Size = new Size(110, 120);

            pictureBox2.Image = Image.FromFile("arrow-min.png");
            pictureBox2.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBox2.Size = new Size(30, 40);

            pictureBox3.Image = Image.FromFile("maximize.png");
            pictureBox3.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBox3.Size = new Size(27, 23);

            pictureBox4.Image = Image.FromFile("close.png");
            pictureBox4.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBox4.Size = new Size(18, 19);
        }
        private void LoadDataIntoDataGridView()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                using (SqlDataAdapter adapter = new SqlDataAdapter("SELECT * FROM CoursesView", connection))
                {
                    DataTable dataTable = new DataTable();
                    adapter.Fill(dataTable);
                    dataGridView1.DataSource = dataTable;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error loading data: " + ex.Message);
            }
        }
        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {

        }

        private void panel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void pictureBox3_Click_1(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            LoadDataIntoDataGridView();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(textBox1.Text) || string.IsNullOrWhiteSpace(textBox2.Text) ||
                    string.IsNullOrWhiteSpace(textBox3.Text) || string.IsNullOrWhiteSpace(textBox4.Text) ||
                    string.IsNullOrWhiteSpace(textBox5.Text) || string.IsNullOrWhiteSpace(textBox6.Text))
                {
                    MessageBox.Show("All fields are required.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                using (SqlConnection connection = new SqlConnection(ConnectionString))
                using (SqlCommand command = new SqlCommand("AddCourse", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@CourseCode", textBox1.Text);
                    command.Parameters.AddWithValue("@InstructorID", textBox5.Text);
                    command.Parameters.AddWithValue("@CreditHours", textBox2.Text);
                    command.Parameters.AddWithValue("@CourseTitle", textBox3.Text);
                    command.Parameters.AddWithValue("@DepartmentName", textBox4.Text);
                    command.Parameters.AddWithValue("@CourseDescription", textBox6.Text);

                    connection.Open();
                    command.ExecuteNonQuery();
                }

                LoadDataIntoDataGridView();

                textBox1.Clear();
                textBox2.Clear();
                textBox3.Clear();
                textBox4.Clear();
                textBox5.Clear();
                textBox6.Clear();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error adding course: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(textBox1.Text) && string.IsNullOrWhiteSpace(textBox2.Text) &&
                    string.IsNullOrWhiteSpace(textBox3.Text) && string.IsNullOrWhiteSpace(textBox4.Text) &&
                    string.IsNullOrWhiteSpace(textBox5.Text))
                {
                    MessageBox.Show("No fields provided for update.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                int courseId = GetCourseIdByCode(textBox1.Text); // Fetch CourseId by CourseCode

                if (courseId == -1)
                {
                    MessageBox.Show("No course found with the provided Course Code.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                UpdateCourse(courseId);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error updating course: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void UpdateCourse(int courseId)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                using (SqlCommand command = new SqlCommand("UpdateCourse", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@CourseId", courseId); // Pass CourseId to the stored procedure
                    command.Parameters.AddWithValue("@InstructorID", string.IsNullOrWhiteSpace(textBox5.Text) ? DBNull.Value : (object)textBox5.Text);
                    command.Parameters.AddWithValue("@CreditHours", string.IsNullOrWhiteSpace(textBox2.Text) ? DBNull.Value : (object)textBox2.Text);
                    command.Parameters.AddWithValue("@CourseTitle", string.IsNullOrWhiteSpace(textBox3.Text) ? DBNull.Value : (object)textBox3.Text);
                    command.Parameters.AddWithValue("@DepartmentName", string.IsNullOrWhiteSpace(textBox4.Text) ? DBNull.Value : (object)textBox4.Text);
                    command.Parameters.AddWithValue("@CourseDescription", string.IsNullOrWhiteSpace(textBox6.Text) ? DBNull.Value : (object)textBox6.Text);

                    connection.Open();
                    command.ExecuteNonQuery();
                }

                LoadDataIntoDataGridView();

                textBox1.Clear();
                textBox2.Clear();
                textBox3.Clear();
                textBox4.Clear();
                textBox5.Clear();
                textBox6.Clear();

                MessageBox.Show("Course updated successfully.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error updating course: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }



        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(textBox1.Text))
                {
                    MessageBox.Show("Please provide a Course Code for deletion.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                int courseId = GetCourseIdByCode(textBox1.Text); // Fetch CourseId by CourseCode

                if (courseId == -1)
                {
                    MessageBox.Show("No course found with the provided Course Code.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                DeleteCourse(courseId);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error deleting course: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            // Check if the clicked column is the "DELETE" column
            if (e.RowIndex >= 0 && dataGridView1.Columns[e.ColumnIndex].Name == "DELETE")
            {
                string courseCode = dataGridView1.Rows[e.RowIndex].Cells["Course_Code"].Value.ToString();
                int courseId = GetCourseIdByCode(courseCode); // Fetch CourseId by CourseCode

                if (courseId == -1)
                {
                    MessageBox.Show("No course found with the provided Course Code.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                DeleteCourse(courseId);
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
        }



        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                DataGridViewRow row = dataGridView1.SelectedRows[0];
                textBox1.Text = row.Cells["Course_Code"].Value.ToString();
                textBox2.Text = row.Cells["Credit_Hours"].Value.ToString();
                textBox3.Text = row.Cells["Course_Title"].Value.ToString();
                textBox4.Text = row.Cells["DepartmentName"].Value.ToString();
                textBox5.Text = row.Cells["InstructorID"].Value.ToString();
                textBox6.Text = row.Cells["Course_Description"].Value.ToString();
            }
        }
        private int GetCourseIdByCode(string courseCode)
        {
            int courseId = -1; // Default value if course is not found

            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                using (SqlCommand command = new SqlCommand("SELECT CourseId FROM Courses WHERE Course_Code = @CourseCode", connection))
                {
                    command.Parameters.AddWithValue("@CourseCode", courseCode);
                    connection.Open();
                    object result = command.ExecuteScalar();

                    if (result != null && result != DBNull.Value)
                    {
                        courseId = Convert.ToInt32(result);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error fetching CourseId: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return courseId;
        }
        private void DeleteCourse(int courseId)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                using (SqlCommand command = new SqlCommand("DeleteCourse", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@CourseId", courseId);

                    connection.Open();
                    int rowsAffected = command.ExecuteNonQuery();

                    if (rowsAffected > 0)
                    {
                        // Course deleted successfully from the database
                        LoadDataIntoDataGridView(); // Refresh data in the DataGridView
                        MessageBox.Show("Course deleted successfully.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        // No course found with the provided Course Id
                        MessageBox.Show("No course found with the provided Course Id.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error deleting course: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


    }
}
