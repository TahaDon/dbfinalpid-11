﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StudentAcademicPerformance
{
    public partial class AdvisorMenu : Form
    {
        public AdvisorMenu()
        {
            InitializeComponent();
            pictureBox1.Image = Image.FromFile("discussion-min.png");
            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBox1.Width = 110;
            pictureBox1.Height = 120;

            pictureBox2.Image = Image.FromFile("arrow-min.png");
            pictureBox2.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBox2.Width = 30;
            pictureBox2.Height = 40;


            pictureBox3.Image = Image.FromFile("maximize.png");
            pictureBox3.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBox3.Width = 18;
            pictureBox3.Height = 19;

            pictureBox4.Image = Image.FromFile("close.png");
            pictureBox4.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBox4.Width = 27;
            pictureBox4.Height = 25;

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void AdvisorMenu_Load(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {

        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void pictureBox3_Click_1(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
        }
    }
}
