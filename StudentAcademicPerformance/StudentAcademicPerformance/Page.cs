﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StudentAcademicPerformance
{
    public partial class Page : Form
    {
        public Page()
        {
            InitializeComponent();
            pictureBox2.Image = Image.FromFile("db1.png");
            pictureBox2.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBox2.Width = 110;
            pictureBox2.Height = 120;

            pictureBox3.Image = Image.FromFile("db2.png");
            pictureBox3.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBox3.Width = 110;
            pictureBox3.Height = 120;

            pictureBox1.Image = Image.FromFile("dashboard.png");
            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBox1.Width = 250;
            pictureBox1.Height = 150;

            pictureBox4.Image = Image.FromFile("maximize.png");
            pictureBox4.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBox4.Width = 27;
            pictureBox4.Height = 23;


            pictureBox5.Image = Image.FromFile("close.png");
            pictureBox5.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBox5.Width = 20;
            pictureBox5.Height = 19;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            panel2.Width += 6;

            if  (panel2.Width >= 750)
            {
                timer1.Stop();

                LoginForm l1 = new LoginForm ();
                l1.Show();
                this.Hide();
            }
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {
           
        }

        private void Page_Load(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {
            
        }

        private void pictureBox5_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
        }
    }
}
