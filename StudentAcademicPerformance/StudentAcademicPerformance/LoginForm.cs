﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StudentAcademicPerformance
{
    public partial class LoginForm : Form
    {
        public LoginForm()
        {
            InitializeComponent();
            pictureBox2.Image = Image.FromFile("arrow-min.png");
            pictureBox2.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBox2.Width = 30;
            pictureBox2.Height = 40;

            pictureBox4.Image = Image.FromFile("close.png");
            pictureBox4.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBox4.Width = 18;
            pictureBox4.Height = 19;

            pictureBox3.Image = Image.FromFile("maximize.png");
            pictureBox3.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBox3.Width = 27;
            pictureBox3.Height = 25;



        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //try
            //{
            //    var con = Configuration.getInstance().getConnection();

            //    string username = textBox1.Text;
            //    string password = textBox2.Text;
            //    string role = comboBox1.Text;

            //    SqlCommand cmd = new SqlCommand("SELECT COUNT(1) FROM SystemUser WHERE UserName=@username AND Password=@password AND Role=@role", con);
            //    cmd.Parameters.AddWithValue("@username", username);
            //    cmd.Parameters.AddWithValue("@password", password);
            //    cmd.Parameters.AddWithValue("@role", role);

            //    int result = Convert.ToInt32(cmd.ExecuteScalar());

            //    if (result == 1)
            //    {
            //        MessageBox.Show("Successful login: " + role);
            //        //NavigateToRoleForm(role);
            //    }
            //    else
            //    {
            //        MessageBox.Show("Error Logging In!");
            //    }
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show("Error: " + ex.Message);


            //StuMenu1 ins = new StuMenu1();
            //ins.Show();
            //this.Hide();

            //ManageCourses ins = new ManageCourses();
            AssignAdvisor ins = new AssignAdvisor();
            ins.Show();
            this.Hide();

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            textBox2.PasswordChar = checkBox1.Checked ? '\0' : '*';
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
        }
    }
}
